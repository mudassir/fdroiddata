Categories:System
License:GPLv3
Web Site:https://www.secuso.informatik.tu-darmstadt.de/en/secuso-home/
Source Code:https://github.com/SecUSo/privacy-friendly-torchlight
Issue Tracker:https://github.com/SecUSo/privacy-friendly-torchlight/issues

Auto Name:Torchlight
Summary:Use LED flash as torchlight
Description:
Simple application to use the phones flash light as a torchlight. It belongs to
the group of Privacy Friendly Apps, developed at the University of Darmstadt,
Germany.
.

Repo Type:git
Repo:https://github.com/SecUSo/privacy-friendly-torchlight

Build:1.1,2
    commit=c417d0ddff4c89640e41532cfce01414e0951db8
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.1
Current Version Code:2
