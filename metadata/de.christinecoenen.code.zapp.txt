Categories:Multimedia
License:MIT
Author Name:Christine Coenen
Web Site:
Source Code:https://github.com/cemrich/zapp
Issue Tracker:https://github.com/cemrich/zapp/issues

Auto Name:Zapp
Summary:Collection of German public broadcasting live streams
Description:
Zapp gives you easy access to a large number of German public broadcasting live
streams. Watch ARD, ZDF and other channels live. Zapp makes it easy to quickly
switch between channels.
.

Repo Type:git
Repo:https://github.com/cemrich/zapp

Build:0.1.0,1
    commit=v0.1.0
    subdir=app
    gradle=yes

Build:0.2.0,2
    commit=v0.2.0
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.2.0
Current Version Code:2
